#include <iostream>
#include <omp.h>


int main(int argc, char** argv)
{
  omp_lock_t writelock;

  omp_init_lock(&writelock);

#pragma omp parallel num_threads(3)
  {
    omp_set_lock(&writelock);
    std::cout << "Parallel region 1: tid:" << omp_get_thread_num() << std::endl;
    omp_unset_lock(&writelock);
  }
  
#pragma omp parallel num_threads(4)
  {
    omp_set_lock(&writelock);
    std::cout << "Parallel region 2: tid:" << omp_get_thread_num() << std::endl;
    omp_unset_lock(&writelock);
  }
    
#pragma omp parallel num_threads(2)
  {
    omp_set_lock(&writelock);
    std::cout << "Parallel region 3: tid:" << omp_get_thread_num() << std::endl;
    omp_unset_lock(&writelock);
  }

  return 0;
}
