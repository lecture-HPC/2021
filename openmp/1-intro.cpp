#include <iostream>
#include <omp.h>


int main(int argc, char** argv)
{

#pragma omp parallel num_threads(3)
  std::cout << "Parallel region 1: tid:" << omp_get_thread_num() << std::endl;

#pragma omp parallel num_threads(4)
  std::cout << "Parallel region 2: tid:" << omp_get_thread_num() << std::endl;

#pragma omp parallel num_threads(2)
  std::cout << "Parallel region 3: tid:" << omp_get_thread_num() << std::endl;

  return 0;
}
