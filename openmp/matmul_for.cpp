void matmul_for(
  int N, int BS,
  float A[N][N], float B[N][N], float C[N][N]
)
{
  int i, j, k;
#pragma omp parallel for collapse(2)
  for (i = 0; i < N; ++i) {
    for (j = 0; j < N; ++j) {
      for (k = 0; k < N; ++k) {
       C[i][j] = C[i][j] + A[i][k] * B[k][j];
      }
    }
  }
}
