#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

double f(double x);

int main(int argc, char** argv)
{
  int n = atoi(argv[1]);
  double fH = 1.0/((double)n);
  double fX, fSum;
  int i;
  
  for (int iter=0; iter<4; ++iter)
  {
    double t0 = omp_get_wtime();
    for (i = 0; i < n; i++)
    {
      fX = fH * ((double)i + 0.5);
      fSum += f(fX);
    }
    double t1 = omp_get_wtime();
    printf("Pi: %2.15f\n", fH * fSum);
    printf("Time: %f\n", t1-t0);
  }
}
