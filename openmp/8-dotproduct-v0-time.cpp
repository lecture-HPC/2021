#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#define MAX_THREADS 128

double T = 0.0;
float dotproduct(float *x, float *y, int n)
{
  int i;
  int nthreads = 0;
  float d[MAX_THREADS];
  float r;
  double t0 = omp_get_wtime();
  #pragma omp parallel shared(x,y,d, nthreads), private(i), firstprivate(n)
  {
    #pragma omp master
    nthreads = omp_get_num_threads();
    
    int tid = omp_get_thread_num();
    d[tid]=0;
    #pragma omp for schedule(static)
    for (i=0; i<n; i++) {
      d[tid] += x[i]*y[i];
    }
  }
  for (i=0; i<nthreads; ++i)
    r+=d[i];
  double t1 = omp_get_wtime();
  printf("Time: %f\n", t1-t0);
  T += t1-t0;
  return r;
}

int main(int argc, char** argv)
{
  int N = atoi(argv[1]);
  float* x = new float[N];
  float* y = new float[N];

  #pragma omp parallel for schedule(static)
  for (int i=0; i<N; i++) {
    x[i] = 1.0/double(i);
    y[i] = N/double(i);
  }

  for (int i=0; i<10; ++i)
  {
    float d = dotproduct(x,y,N);
  }
  printf("Avrg: %f\n", T/10);
  return 0;
}

