#include <iostream>
#include <omp.h>

int fibonacci(int n)
{
  if (n<2)
   return n;

  int sum;
  int r1;
  int r2;
  #pragma omp task if (n>20) depend(out:r1) shared(r1) firstprivate(n)
    r1 = fibonacci(n-1);
  #pragma omp task if (n>20) depend(out:r2) shared(r2) firstprivate(n)
    r2 = fibonacci(n-2);
  #pragma omp task if (n>20) depend(out:sum) depend(in:r1,r2) shared(r1,r2,sum)
    sum = r1 + r2;
  #pragma omp taskwait
  return sum;
}

int main(int argc, char** argv)
{
  int r, n;
  n = atoi(argv[1]);
  double t0 = omp_get_wtime();
#pragma omp parallel shared(r) firstprivate(n)
  {
#pragma omp master
    {
#pragma omp task depend(out:r) firstprivate(n) shared(r)
      r = fibonacci( n );
#pragma omp task depend(in:r) firstprivate(n) shared(r)
      {
        double t1 = omp_get_wtime();
        std::cout <<"Fibonacci(" << n << ") =" << r << std::endl;
        std::cout << "Time: " << t1-t0 << std::endl;
      }
    }
  }

  return 0;
}
