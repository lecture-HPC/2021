#include <iostream>
#include <omp.h>

int main(int argc, char** argv)
{
  #pragma omp parallel num_threads(3)
  {
    #pragma omp master
    /* static scheduler with chunk_size == 1 */
    for (int i=0; i<16; ++i)
    {
      #pragma omp task
      {
        #pragma omp critical
        std::cout << omp_get_thread_num() << ":: do iteration " << i << std::endl;
      }
    }
    #pragma omp taskwait
  }
  return 0;
}

