#include <iostream>
#include <omp.h>

int fibonacci(const int n)
{
  if (n<2)
    return n;
  else {
    int r1,r2;
#pragma omp task if (n>10) shared(r2) firstprivate(n)
    r2 = fibonacci( n-2 );

    r1 = fibonacci( n-1 );
#pragma omp taskwait
    return r1 + r2;
  }
}

int main(int argc, char** argv)
{
  int r, n;
  n = atoi(argv[1]);
  double t0 = omp_get_wtime();
#pragma omp parallel shared(r) firstprivate(n)
  {
#pragma omp master
    {
#pragma omp task shared(r) firstprivate(n)
      r = fibonacci( n );
    }
  }
  double t1 = omp_get_wtime();
  std::cout <<"Fibonacci(" << n << ") =" << r << std::endl;
  std::cout << "Time: " << t1-t0 << std::endl;

  return 0;
}

