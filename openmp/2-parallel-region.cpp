#include <iostream>
#include <omp.h>

int main(int argc, char** argv)
{
  int last = -1;
  
#pragma omp parallel
  {
    int tid = omp_get_thread_num();
    int num_threads = omp_get_num_threads();
    std::cout << "Parallel region 1: tid:" << tid << "/num_threads" << std::endl;
    last = tid;
  }
  std::cout << "Last thread is:" << last << std::endl;

  return 0;
}
