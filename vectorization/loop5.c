double dotprod(int n, double* A, double* B)
{
  double retval = 0;
#pragma omp simd reduction(+: retval)
  for (int i = 0; i < n; i++)
    retval += A[i] * B[i];
  return retval;
}
