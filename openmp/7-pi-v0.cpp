#include <iostream>
#include <random>
#include <chrono>
#include <omp.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
  long Nc=0, N=0;
  int P = 0;

  N = atoi(argv[1]);

  /* uniform distribution U(0,1) */
  std::uniform_real_distribution<float> distribution(0,1);
  std::default_random_engine generator;

  auto t0 = std::chrono::steady_clock::now();
  #pragma omp parallel shared(Nc,P) firstprivate(N)
  {
    P = omp_get_num_threads();

    #pragma omp for reduction(+:Nc)
    for (long i=0; i<N; ++i)
    {
      double x,y;
      #pragma omp critical
      {
        x= distribution(generator);
        y= distribution(generator);
      }
      double d = x*x+y*y;
      if (d<=1)++Nc;
    }
  }
  auto t1 = std::chrono::steady_clock::now();
  std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t1 - t0);

  std::cout << "Pi ~ " << 4*double(Nc)/double(N) << std::endl;
  std::cout << "Tp= " << time_span.count() << " s" << std::endl;
  std::cout << "P = " << P << std::endl;
  
  return 0;
}

