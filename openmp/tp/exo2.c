#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <omp.h>

/* utility function */
void mat_print(int M, int N, double *mat) {

  for (int i =0; i < N; ++i) {
    for (int j = 0; j < M; ++j) {
      printf("%f ", mat[i*N+j]);
    }
    printf("\n");
  }
  printf("\n");
}


/* A(i,j)<- i+j */
void mat_fill(int M, int N, double *A) {

  for (int i =0; i < N; ++i) {
    for (int j = 0; j < M; ++j) {
      A[i*N+j] = i+j;
    }
  }
}

/* A(i,j)<- 0 */
void mat_zero(int M, int N, double *A) {

  for (int i =0; i < N; ++i) {
    for (int j = 0; j < M; ++j) {
      A[i*N+j] = 0;
    }
  }
}

/* A(i,j)<- shift */
void mat_shift(int M, int N, double *A) {

  for (int i =0; i < N; ++i) {
    for (int j = 0; j < M; ++j) {
      A[i*N+j] = j == (i+1)%M ? 1 : 0;
    }
  }
}


/* A(i,j)<- random */
void mat_rand(int M, int N, double *A) {

  for (int i =0; i < N; ++i) {
    for (int j = 0; j < M; ++j) {
      A[i*N+j] = drand48();
    }
  }
}


/* 1 iff A==B, else 0 */
int mat_is_eq(int M, int N, double *A, double *B) {

  for (int i =0; i < N; ++i) {
    for (int j = 0; j < M; ++j) {
      if (A[i*N+j] != B[i*N+j]) return 0;
    }
  }
  return 1;
}


/* compute C <- A*B */
void mat_mm(int M, int N, int K, double *A, double *B, double *C)
{
}


/* compute C <- A*B */
void mat_mm_par(int M, int N, int K, double *A, double *B, double *C)
{
}


/* compute C <- A*B */
void matb_mm(int b, int M, int N, int K, double *A, double *B, double *C)
{
}


/* compute C <- A*B */
void matb_mm_par(int b, int M, int N, int K, double *A, double *B, double *C)
{
}


/* compute C <- A*B */
void matb_mm_task(int b, int M, int N, int K, double *A, double *B, double *C)
{
}

/* compute C <- A*B */
void matb_mm_task_strassen(int M, int N, int K, double *A, double *B, double *C)
{
}


int main(int argc, char** argv)
{
  double t0,t1;
  int M, N, K; /* matrix dimension */
  int b; /* block size */
  if (argc >1)
    M=N=K=atoi(argv[1]);
  else
    M=N=K=4;

  if (argc >2)
    b = atoi(argv[2]);
  else
    b = 0;

  if (b>0)
  {
    M = (M+b-1)/b*b;
    N = (N+b-1)/b*b;
    K = (K+b-1)/b*b;
  }

  printf("M:%i, N:%i, K:%i\n", M,N,K);
  printf("Blocking factor:%i\n", b);

  double *A = (double*)malloc(sizeof(double)*M*K);
  double *B = (double*)malloc(sizeof(double)*K*N);
  double *Cseq = (double*)malloc(sizeof(double)*M*N);
  double *Cpar = (double*)malloc(sizeof(double)*M*N);
  double *Cb = (double*)malloc(sizeof(double)*M*N);

  mat_shift(M, K, A);
  mat_rand(K, N, B);

  if (M*N*K <= 64)
  {
    printf("A:\n");
    mat_print(M,K,A);
    printf("B:\n");
    mat_print(K,N,B);
  }

  /* reset C */
  mat_zero(M, N, Cseq);
  mat_mm(M,N,K,A,B,Cseq);
  printf("Cseq !\n");

  if (M*N*K <= 64)
    mat_print(M,N,Cseq);

  /* reset C */
  mat_zero(M, N, Cpar);
  mat_mm_par(M,N,K,A,B,Cpar);
  printf("Cpar !!!\n");

  if (M*N*K <= 64)
    mat_print(M,N,Cpar);

  if (b >0)
  {
    /* reset C */
    mat_zero(M, N, Cb);
    matb_mm(b,M,N,K,A,B,Cb);
    printf("Cblock!!!\n");

    if (M*N*K <= 64)
      mat_print(M,N,Cb);

    /* reset C */
    mat_zero(M, N, Cb);
    matb_mm_par(b,M,N,K,A,B,Cb);
    printf("Cblock par !!!");

    if (M*N*K <= 64)
      mat_print(M,N,Cb);

    /* reset C */
    mat_zero(M, N, Cb);
    matb_mm_task(b,M,N,K,A,B,Cb);
    printf("Cblock Task !!!\n");

    if (M*N*K <= 64)
      mat_print(M,N,Cb);

    /* reset C */
    mat_zero(M, N, Cb);
    matb_mm_task_strassen(M,N,K,A,B,Cb);
    printf("Cblock Task Strassen!!!\n");

    if (M*N*K <= 64)
      mat_print(M,N,Cb);
  }

  free(Cb);
  free(Cpar);
  free(Cseq);
  free(B);
  free(A);
  return 0;
}
