#include <omp.h>
#define MAX_THREADS 128

float dotproduct(float *x, float *y, int n)
{
  int i;
  int nthreads = 0;
  float d[MAX_THREADS];
  float r;
  #pragma omp parallel shared(x,y,d, nthreads), private(i), firstprivate(n)
  {
    #pragma omp master
    nthreads = omp_get_num_threads();
    
    int tid = omp_get_thread_num();
    d[tid]=0;
    #pragma omp for
    for (i=0; i<n; i++) {
      d[tid] += x[i]*y[i];
    }
  }
  for (i=0; i<nthreads; ++i)
    r+=d[i];
  return r;
}

