float dotproduct(float *x, int *y, int n)
{
  int i;
  float d;
#pragma omp parallel for private(i) shared(x, y) firstprivate(n) \
        reduction(+:d)
  for (i=0; i<n; i++) {
    d += x[i]*y[i];
  }
  return d;
}
