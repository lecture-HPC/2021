#include <iostream>
#include <random>
#include <omp.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
  long Nc=0, N=0;
  int P = 0;

  N = atoi(argv[1]);

  double t0 = omp_get_wtime();
  #pragma omp parallel shared(Nc,P) firstprivate(N)
  {
    P = omp_get_num_threads();
    std::uniform_real_distribution<float> distribution(0,1);
    std::mt19937 generator;

    #pragma omp for reduction(+:Nc)
    for (long i=0; i<N; ++i)
    {
      double x= distribution(generator);
      double y= distribution(generator);
      double d = x*x+y*y;
      if (d<=1)++Nc;
    }
  }
  double t1 = omp_get_wtime();

  std::cout << "Pi ~ " << 4*double(Nc)/double(N) << std::endl;
  std::cout << "Tp= " << (t1-t0) << " s" << std::endl;
  std::cout << "P = " << P << std::endl;
  
  return 0;
}

