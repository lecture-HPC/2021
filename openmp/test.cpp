#include <omp.h>
#include <iostream>

int main()
{
  int nthreads;
#pragma omp parallel
#pragma omp master
  nthreads = omp_get_num_threads();
  
  std::cout << "Number of threads for the next parallel region: "
            << nthreads << std::endl;
  return 0;
}
