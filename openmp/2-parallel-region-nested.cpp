#include <iostream>
#include <omp.h>

int main(int argc, char** argv)
{
#pragma omp parallel num_threads(3)
  {
    int tid0 = omp_get_thread_num();
    #pragma omp parallel num_threads(2) firstprivate(tid0)
    {
      int tid1 = omp_get_thread_num();
      #pragma omp critical
      std::cout << "Parallel region : " << tid0 << "/" << tid1 << std::endl;
    }
  }
  return 0;
}
