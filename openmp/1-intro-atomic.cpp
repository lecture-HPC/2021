#include <iostream>
#include <cstring>
#include <omp.h>

int main(int argc, char** argv)
{
  char buffer[256]; /* upper bound */
  const int len = strlen("Parallel region 1: tid:_\n"); /* _ = id */
  int pos =0;

  pos= 0;
#pragma omp parallel num_threads(8) shared(pos)
{
  int local_pos;
  #pragma omp atomic capture
  local_pos = pos++;
  /* copy string at the right location + update the id */ 
  memcpy(buffer+len*local_pos,"Parallel region 2: tid:_\n", len);
  buffer[len*local_pos+23] = '0'+ omp_get_thread_num();
}
  std::cout << buffer << std::flush;

  return 0;
}
