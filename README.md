## This is the gitlab for lectures on HPC at INSA de Lyon in 2021

## Schedule:

### 2021/10/5 Introduction and Parallel architecture
[Slides: Introduction](0-Intro.pdf)

[Slides: Parallel architecture](1-archi.pdf)

### 2021/10/12 Lecture on parallel algorithms in the morning. Afternoon: TD on parallel algorithms

[Slides: Parallel algorithm](2-algopar.pdf)
Note: I made some mistakes (figures, codes,...) and I need to check slides. 
A updated version will be pushed by ~~the end of the week~~ ~~monday the 18th october~~ ~~**as soon as possible this week!**~~ It is on the top of my todo list :-)

[Slides: TD Parallel algorithm](2-TD-algopar.pdf)

### 2021/10/19 Lecture on OpenMP in the morning. Afternoon: TD on OpenMP + some advanced features

[Slides: OpenMP](3-openmp.pdf)
Codes are available in directory openmp.
The subdirectory openmp/tp contains a TP if you want to work on it.

### 2021/11/09 Lecture on Vectorization in the morning. Afternoon: Introduction to GPU

[Slides: Vectorization](4-vectorization.pdf)
Codes are available in directory vectorization. Micro TP: vectorization of pi.c (with f.c).

[Slides: GPU Introduction](5_GPU_0_introduction.pdf)

### 2021/11/16 Lecture on GPU in the morning
~~[Slides: GPU Optimisation](6_GPU_1_optimize.pdf)~~ [New slides:](6-GPU_inside.pdf)
