#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <omp.h>


/* */
void Q1(void)
{
}


/* */
int Q2(void)
{
}



/* */
int Q3(void)
{
}


/* n: nombre d'éléments du tableau a[0],...,a[n-1] */
long Q4(int n, long* a)
{
}

/* n: nombre d'éléments du tableau a[0],...,a[n-1] */
long Q4atomic(int n, long* a)
{
}


int main(int argc, char** argv)
{
  double t0, t1;

  printf("-------------- Q1\n");
  #pragma omp parallel
  Q1();

/* à supprimer si question précédente traitée */
return 1;

  printf("\n-------------- Q2\n");
  int num_threads = Q2();
  printf("Le nombre de threads par défaut des régions parallèles est: %i\n", num_threads);

/* à supprimer si question précédente traitée */
return 1;

  printf("\n-------------- Q3\n");
  int per_thread_value[num_threads];
  #pragma omp parallel
  {
    per_thread_value[omp_get_thread_num()] = Q3();
  }
  /* verification ! */
  for (int i=1; i<num_threads; ++i)
  {
    if (per_thread_value[i] != per_thread_value[0])
    {
      fprintf(stderr,"Error: thread %i a retourné la valeur '%i' différente"
                     " du thread 0 '%i'\n",
                     i, per_thread_value[i], per_thread_value[0]);
      return -1;
    }
  }
  printf("Valeur retournée par tous les threads: '%i'\n",per_thread_value[0]);

/* à supprimer si question précédente traitée */
return 1;


  printf("\n-------------- Q4\n");
  int n=10240;
  long* array = (long*)malloc(sizeof(long)*n);
  long sum;

  for (int i=0; i<n; ++i)
    array[i] = i;

  sum = Q4(n, array);
  printf("Résultat de la réduction: %li\n", sum );

/* à supprimer si question précédente traitée */
return 1;


  printf("\n-------------- Q4 atomic\n");

  sum = Q4atomic(n, array);
  printf("Résultat de la réduction: %li\n", sum );

/* à supprimer si question précédente traitée */
return 1;

  free(array);
  return 0;
}
