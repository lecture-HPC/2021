void loop(int n, double* restrict C, double* restrict A, double alpha, double* restrict B, double beta)
{
  for (int i = 0; i < n; i++)
    C[i] = alpha * A[i] + beta * B[i];
}
