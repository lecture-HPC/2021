void loop(int n, double* C, double* A, double alpha, double* B, double beta)
{
  for (int i = 0; i < n; i++)
    C[i] = alpha * A[i] + beta * B[i];
}
