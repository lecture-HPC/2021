#include <iostream>
#include <omp.h>

int main(int argc, char** argv)
{
  #pragma omp parallel
  {
    int tid = omp_get_thread_num();
    #pragma omp masked filter( (tid%2 ==0 ? tid : -1)
    #pragma omp critical
    std::cout << "Do masked instruction (" << tid << ")" << std::endl;
  }

  return 0;
}
