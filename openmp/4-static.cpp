#include <iostream>
#include <omp.h>

int main(int argc, char** argv)
{
  #pragma omp parallel num_threads(3)
  {
    int tid = omp_get_thread_num();
    /* static scheduler with chunk_size == 1 */
    #pragma omp for schedule(static, 1)
    for (int i=0; i<16; ++i)
    {
      #pragma omp critical
      std::cout << tid << ":: do iteration " << i << std::endl;
    }
  }
  return 0;
}

