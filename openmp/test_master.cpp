#include <omp.h>
#include <iostream>

int main()
{
  std::cout << "I'm thread " <<  omp_get_thread_num()
            << " in a group of " << omp_get_num_threads()
            << " thread(s)" << std::endl;
  return 0;
}
