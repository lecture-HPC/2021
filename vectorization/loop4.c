void loop(int n, double* C, double* A, double alpha, double* B, double beta)
{
#pragma omp simd safelen(16) simdlen(8) aligned(C,A,B:32)
  for (int i = 0; i < n; i++)
    C[i] = alpha * A[i] + beta * B[i];
}
