int loop(int n, double* A, double* B)
{
  #pragma omp simd safelen(2)
  for(int i=2 ; i < n ; i++)
  {
    A[i]=A[i-2]+B[i];
  }
}
